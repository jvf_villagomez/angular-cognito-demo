import { Component, OnInit } from '@angular/core';
import { Auth, Hub } from 'aws-amplify';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
    
  }

  async userInfo() {
    try {
      const currentUserInfo = await Auth.currentUserInfo()
      console.log(currentUserInfo);
    } catch (err) {
      console.log('error fetching user info: ', err);
    }
  }

  async userSession() {
    try {
      const currentUserSession = await Auth.currentSession()
      console.log(currentUserSession);
    } catch (err) {
      console.log('error fetching user info: ', err);
    }
  }

  async userAuthUser() {
    try {
      const currentAuthUser = await Auth.currentAuthenticatedUser()
      console.log(currentAuthUser);
    } catch (err) {
      console.log('error fetching user info: ', err);
    }
  }

  async userLogoutUser() {
    try {
      await Auth.signOut({ global: true })
      console.log('loggin out current user');
      window.location.href = "https://cxhotelingdev.auth.us-east-1.amazoncognito.com/logout?client_id=5liikkkv9s92kjmgjj3m3aklcm&logout_uri=http://localhost:5001/"
    } catch (err) {
      console.log('error signing out: ', err);
    }
  }


}
