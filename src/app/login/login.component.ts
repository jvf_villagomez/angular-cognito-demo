import { Component, OnInit, Input, inject, Inject } from '@angular/core';
import { Auth, Hub } from 'aws-amplify';
import { Router } from '@angular/router';
import { FormsModule } from '@angular/forms';
//import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})

export class LoginComponent implements OnInit {
  username: string = '';
  email: string = '';
  password: string = '';


  constructor(
    private router: Router
    //private spinner: NgxSpinnerService
    ) { 

    //currentAuthenticatedUser: when user comes to login page again
    Auth.currentAuthenticatedUser()
      .then(() => {
        this.router.navigate(['/home'], { replaceUrl: true });
      }).catch((err) => {
        console.log(err);
      })

  }
  ngOnInit(): void {
  }

  async loginWithCognito() {
    try {
      var user = await Auth.signIn(this.username.toString(), this.password.toString());
      console.log('Authentication performed for user=' + this.username + 'password=' + this.password + ' login result==' + user);
      var tokens = user.signInUserSession;
      if (tokens != null) {
        console.log('User authenticated');
        this.router.navigate(['home']);
        alert('You are logged in successfully !');
      }
    } catch (error) {
      console.log(error);
      alert('User Authentication failed');
    }
  }

  async loginWithFederatedCognito() {
    try {
      var user = await Auth.federatedSignIn({customProvider: 'CEMEXLAB'});
      

      // Used for listening to login events
      Hub.listen("auth", ({ payload: { event, data } }) => {
        if (event === "signedIn") {
          console.log(event);
          this.router.navigate(['home']);
          alert('You are logged in successfully !');
        }
      });
    } catch (error) {
      console.log(error);
      alert('User Authentication failed');
    }
  }

}