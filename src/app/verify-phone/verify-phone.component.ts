import { Component, OnInit } from '@angular/core';
import {Auth} from 'aws-amplify';
import { FormsModule } from '@angular/forms'; 
import { Router } from '@angular/router';

@Component({
  selector: 'app-verify-phone',
  templateUrl: './verify-phone.component.html',
  styleUrls: ['./verify-phone.component.css']
})
export class VerifyPhoneComponent implements OnInit {
  employeeNumber!:string;
  verificationCode!: string;

  constructor(private router:Router) { }
  ngOnInit(): void {
  }

  verify(){
    try {
      const user = Auth.confirmSignUp(
        this.employeeNumber,
        this.verificationCode);
      console.log({ user });
      alert('Phone verification completed');
      this.router.navigate(['login']);
    } catch (error) {
      console.log('error with verification code:', error);
    }
  }

}
